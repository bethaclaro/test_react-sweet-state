import {createStore, createHook} from 'react-sweet-state';
import {toggleVisible} from './actions.js';

const Store  = createStore({
  initialState: {
    visible: false
  },
  actions: {
    toggleVisible,
  }
})

export const useAppStore = createHook(Store);