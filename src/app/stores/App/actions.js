export const toggleVisible = () => ({ setState, getState }) => {
  setState({
    visible: !getState().visible
  })
}