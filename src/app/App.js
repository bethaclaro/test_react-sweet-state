import React from 'react';
import './App.css';
import {useAppStore} from './stores';
import {Grommet} from 'grommet';

function App() {
  const [{visible}, {toggleVisible: toggle}] = useAppStore();
  return (
    <div className="App">
      <Grommet plain>
        {`Visible: ${visible}`}
        <button onClick={()=>{
          toggle();
        }}>Toggle</button>
      </Grommet>
    </div>
  );
}

export default App;
